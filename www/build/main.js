webpackJsonp([10],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var PopoverComponent = /** @class */ (function () {
    function PopoverComponent() {
        this.popoverItemChat = [];
        this.popoverItemChat = [
            { itemName: 'View Profile' },
            { itemName: 'Edit Profile' }
        ];
    }
    PopoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'popover',template:/*ion-inline-start:"D:\MSME-Conf\MSME\MSME-Prototype\src\components\popover\popover.html"*/'<!-- Generated template for the PopoverComponent component -->\n<ion-list  *ngFor="let item of popoverItemChat" no-padding>\n    <ion-item no-lines >\n        {{item.itemName}}\n      </ion-item>\n</ion-list>\n\n\n'/*ion-inline-end:"D:\MSME-Conf\MSME\MSME-Prototype\src\components\popover\popover.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], PopoverComponent);
    return PopoverComponent;
}());

//# sourceMappingURL=popover.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GrievancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GrievancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GrievancePage = /** @class */ (function () {
    function GrievancePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    GrievancePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GrievancePage');
    };
    GrievancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-grievance',template:/*ion-inline-start:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\grievance\grievance.html"*/'<!--\n  Generated template for the GrievancePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="darkmode">\n    <ion-title padding-left>Grievance</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-list>\n        <ion-item color="darkmode">\n          <ion-label stacked>Name</ion-label>\n          <ion-input type="text"></ion-input>\n        </ion-item>\n      \n        <ion-item color="darkmode">\n          <ion-label stacked>MSME Name</ion-label>\n          <ion-input type="text"></ion-input>\n        </ion-item>\n        <ion-item color="darkmode">\n            <ion-label stacked>Mobile Number</ion-label>\n            <ion-input type="text"></ion-input>\n          </ion-item>\n          <ion-item color="darkmode">\n              <ion-label stacked>Email ID</ion-label>\n              <ion-input type="text"></ion-input>\n            </ion-item>\n            <ion-item color="darkmode">\n                <ion-label stacked>Grievance Category</ion-label>\n                <ion-input type="text"></ion-input>\n              </ion-item>\n              <ion-item color="darkmode">\n                  <ion-label stacked>Grievance Description</ion-label>\n                  <ion-input type="text"></ion-input>\n                </ion-item>\n\n      \n      </ion-list>\n      <button ion-button  color="royal" float-right>Submit</button>\n      <ion-fab right bottom >\n          <button ion-fab color="royal"><ion-icon class="fa fa-comments"></ion-icon></button>\n        </ion-fab>\n</ion-content>\n'/*ion-inline-end:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\grievance\grievance.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]) === "function" && _b || Object])
    ], GrievancePage);
    return GrievancePage;
    var _a, _b;
}());

//# sourceMappingURL=grievance.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TrainingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TrainingPage = /** @class */ (function () {
    function TrainingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TrainingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrainingPage');
    };
    TrainingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-training',template:/*ion-inline-start:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\training\training.html"*/'<!--\n  Generated template for the TrainingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="darkmode">\n    <ion-title padding-left>Training</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card>\n      <ion-card-header>\n        February 2020\n      </ion-card-header>\n      <ion-card-content>\n        <ion-grid>\n          <ion-row class="header">\n            <ion-col col-4>Day</ion-col>\n            <ion-col col-8>Event</ion-col>\n          </ion-row>\n          <ion-row>\n              <ion-col col-4>Tue 4</ion-col>\n              <ion-col col-8>CCTM-31st Batch</ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col col-4>Wed 5</ion-col>\n                <ion-col col-8>ACCIQC-11th Batch</ion-col>\n              </ion-row>             \n        </ion-grid>\n      </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <ion-card-header>\n          March 2020\n        </ion-card-header>\n        <ion-card-content>\n          <ion-grid>\n            <ion-row class="header">\n              <ion-col col-4>Day</ion-col>\n              <ion-col col-8>Event</ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col col-4>Tue 4</ion-col>\n                <ion-col col-8>CCTM-31st Batch</ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col col-4>Wed 5</ion-col>\n                  <ion-col col-8>ACCIQC-12th Batch</ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-4>Thur 18</ion-col>\n                    <ion-col col-8>CCAM-78st Batch</ion-col>\n                  </ion-row>\n                  <ion-row>\n                      <ion-col col-4>Fri 19</ion-col>\n                      <ion-col col-8>CCTM-62st Batch</ion-col>\n                    </ion-row>\n          </ion-grid>\n        </ion-card-content>\n      </ion-card>\n      <ion-card>\n          <ion-card-header>\n            April 2020\n          </ion-card-header>\n          <ion-card-content>\n            <ion-grid>\n              <ion-row class="header">\n                <ion-col col-4>Day</ion-col>\n                <ion-col col-8>Event</ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col col-4>Wed 4</ion-col>\n                  <ion-col col-8>CCTM-34st Batch</ion-col>\n                </ion-row>\n            </ion-grid>\n          </ion-card-content>\n        </ion-card>\n</ion-content>\n'/*ion-inline-end:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\training\training.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], TrainingPage);
    return TrainingPage;
}());

//# sourceMappingURL=training.js.map

/***/ }),

/***/ 140:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 140;

/***/ }),

/***/ 181:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/dash-board/dash-board.module": [
		533,
		16
	],
	"../pages/grievance/grievance.module": [
		535,
		15
	],
	"../pages/project/project.module": [
		538,
		14
	],
	"../pages/remote-work/remote-work.module": [
		539,
		13
	],
	"../pages/training/training.module": [
		540,
		12
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 181;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(399);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_emoji_picker__ = __webpack_require__(482);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_chooser__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_opener__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_path__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_native_page_transitions__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__app_component__ = __webpack_require__(530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_dash_board_dash_board__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_project_project__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_remote_work_remote_work__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_grievance_grievance__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_training_training__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_popover_popover__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_dash_board_dash_board__["a" /* DashBoardPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_project_project__["a" /* ProjectPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_remote_work_remote_work__["a" /* RemoteWorkPage */],
                __WEBPACK_IMPORTED_MODULE_19__components_popover_popover__["a" /* PopoverComponent */],
                __WEBPACK_IMPORTED_MODULE_17__pages_grievance_grievance__["a" /* GrievancePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_training_training__["a" /* TrainingPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_6_ionic_emoji_picker__["a" /* EmojiPickerModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/dash-board/dash-board.module#DashBoardPageModule', name: 'DashBoardPage', segment: 'dash-board', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/grievance/grievance.module#GrievancePageModule', name: 'GrievancePage', segment: 'grievance', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/project/project.module#ProjectPageModule', name: 'ProjectPage', segment: 'project', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/remote-work/remote-work.module#RemoteWorkPageModule', name: 'RemoteWorkPage', segment: 'remote-work', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/training/training.module#TrainingPageModule', name: 'TrainingPage', segment: 'training', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_dash_board_dash_board__["a" /* DashBoardPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_project_project__["a" /* ProjectPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_remote_work_remote_work__["a" /* RemoteWorkPage */],
                __WEBPACK_IMPORTED_MODULE_19__components_popover_popover__["a" /* PopoverComponent */],
                __WEBPACK_IMPORTED_MODULE_17__pages_grievance_grievance__["a" /* GrievancePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_training_training__["a" /* TrainingPage */]
            ],
            providers: [
                // ImagePicker,
                //  Crop,  
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 464:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 202,
	"./af.js": 202,
	"./ar": 203,
	"./ar-dz": 204,
	"./ar-dz.js": 204,
	"./ar-kw": 205,
	"./ar-kw.js": 205,
	"./ar-ly": 206,
	"./ar-ly.js": 206,
	"./ar-ma": 207,
	"./ar-ma.js": 207,
	"./ar-sa": 208,
	"./ar-sa.js": 208,
	"./ar-tn": 209,
	"./ar-tn.js": 209,
	"./ar.js": 203,
	"./az": 210,
	"./az.js": 210,
	"./be": 211,
	"./be.js": 211,
	"./bg": 212,
	"./bg.js": 212,
	"./bm": 213,
	"./bm.js": 213,
	"./bn": 214,
	"./bn.js": 214,
	"./bo": 215,
	"./bo.js": 215,
	"./br": 216,
	"./br.js": 216,
	"./bs": 217,
	"./bs.js": 217,
	"./ca": 218,
	"./ca.js": 218,
	"./cs": 219,
	"./cs.js": 219,
	"./cv": 220,
	"./cv.js": 220,
	"./cy": 221,
	"./cy.js": 221,
	"./da": 222,
	"./da.js": 222,
	"./de": 223,
	"./de-at": 224,
	"./de-at.js": 224,
	"./de-ch": 225,
	"./de-ch.js": 225,
	"./de.js": 223,
	"./dv": 226,
	"./dv.js": 226,
	"./el": 227,
	"./el.js": 227,
	"./en-SG": 228,
	"./en-SG.js": 228,
	"./en-au": 229,
	"./en-au.js": 229,
	"./en-ca": 230,
	"./en-ca.js": 230,
	"./en-gb": 231,
	"./en-gb.js": 231,
	"./en-ie": 232,
	"./en-ie.js": 232,
	"./en-il": 233,
	"./en-il.js": 233,
	"./en-nz": 234,
	"./en-nz.js": 234,
	"./eo": 235,
	"./eo.js": 235,
	"./es": 236,
	"./es-do": 237,
	"./es-do.js": 237,
	"./es-us": 238,
	"./es-us.js": 238,
	"./es.js": 236,
	"./et": 239,
	"./et.js": 239,
	"./eu": 240,
	"./eu.js": 240,
	"./fa": 241,
	"./fa.js": 241,
	"./fi": 242,
	"./fi.js": 242,
	"./fo": 243,
	"./fo.js": 243,
	"./fr": 244,
	"./fr-ca": 245,
	"./fr-ca.js": 245,
	"./fr-ch": 246,
	"./fr-ch.js": 246,
	"./fr.js": 244,
	"./fy": 247,
	"./fy.js": 247,
	"./ga": 248,
	"./ga.js": 248,
	"./gd": 249,
	"./gd.js": 249,
	"./gl": 250,
	"./gl.js": 250,
	"./gom-latn": 251,
	"./gom-latn.js": 251,
	"./gu": 252,
	"./gu.js": 252,
	"./he": 253,
	"./he.js": 253,
	"./hi": 254,
	"./hi.js": 254,
	"./hr": 255,
	"./hr.js": 255,
	"./hu": 256,
	"./hu.js": 256,
	"./hy-am": 257,
	"./hy-am.js": 257,
	"./id": 258,
	"./id.js": 258,
	"./is": 259,
	"./is.js": 259,
	"./it": 260,
	"./it-ch": 261,
	"./it-ch.js": 261,
	"./it.js": 260,
	"./ja": 262,
	"./ja.js": 262,
	"./jv": 263,
	"./jv.js": 263,
	"./ka": 264,
	"./ka.js": 264,
	"./kk": 265,
	"./kk.js": 265,
	"./km": 266,
	"./km.js": 266,
	"./kn": 267,
	"./kn.js": 267,
	"./ko": 268,
	"./ko.js": 268,
	"./ku": 269,
	"./ku.js": 269,
	"./ky": 270,
	"./ky.js": 270,
	"./lb": 271,
	"./lb.js": 271,
	"./lo": 272,
	"./lo.js": 272,
	"./lt": 273,
	"./lt.js": 273,
	"./lv": 274,
	"./lv.js": 274,
	"./me": 275,
	"./me.js": 275,
	"./mi": 276,
	"./mi.js": 276,
	"./mk": 277,
	"./mk.js": 277,
	"./ml": 278,
	"./ml.js": 278,
	"./mn": 279,
	"./mn.js": 279,
	"./mr": 280,
	"./mr.js": 280,
	"./ms": 281,
	"./ms-my": 282,
	"./ms-my.js": 282,
	"./ms.js": 281,
	"./mt": 283,
	"./mt.js": 283,
	"./my": 284,
	"./my.js": 284,
	"./nb": 285,
	"./nb.js": 285,
	"./ne": 286,
	"./ne.js": 286,
	"./nl": 287,
	"./nl-be": 288,
	"./nl-be.js": 288,
	"./nl.js": 287,
	"./nn": 289,
	"./nn.js": 289,
	"./pa-in": 290,
	"./pa-in.js": 290,
	"./pl": 291,
	"./pl.js": 291,
	"./pt": 292,
	"./pt-br": 293,
	"./pt-br.js": 293,
	"./pt.js": 292,
	"./ro": 294,
	"./ro.js": 294,
	"./ru": 295,
	"./ru.js": 295,
	"./sd": 296,
	"./sd.js": 296,
	"./se": 297,
	"./se.js": 297,
	"./si": 298,
	"./si.js": 298,
	"./sk": 299,
	"./sk.js": 299,
	"./sl": 300,
	"./sl.js": 300,
	"./sq": 301,
	"./sq.js": 301,
	"./sr": 302,
	"./sr-cyrl": 303,
	"./sr-cyrl.js": 303,
	"./sr.js": 302,
	"./ss": 304,
	"./ss.js": 304,
	"./sv": 305,
	"./sv.js": 305,
	"./sw": 306,
	"./sw.js": 306,
	"./ta": 307,
	"./ta.js": 307,
	"./te": 308,
	"./te.js": 308,
	"./tet": 309,
	"./tet.js": 309,
	"./tg": 310,
	"./tg.js": 310,
	"./th": 311,
	"./th.js": 311,
	"./tl-ph": 312,
	"./tl-ph.js": 312,
	"./tlh": 313,
	"./tlh.js": 313,
	"./tr": 314,
	"./tr.js": 314,
	"./tzl": 315,
	"./tzl.js": 315,
	"./tzm": 316,
	"./tzm-latn": 317,
	"./tzm-latn.js": 317,
	"./tzm.js": 316,
	"./ug-cn": 318,
	"./ug-cn.js": 318,
	"./uk": 319,
	"./uk.js": 319,
	"./ur": 320,
	"./ur.js": 320,
	"./uz": 321,
	"./uz-latn": 322,
	"./uz-latn.js": 322,
	"./uz.js": 321,
	"./vi": 323,
	"./vi.js": 323,
	"./x-pseudo": 324,
	"./x-pseudo.js": 324,
	"./yo": 325,
	"./yo.js": 325,
	"./zh-cn": 326,
	"./zh-cn.js": 326,
	"./zh-hk": 327,
	"./zh-hk.js": 327,
	"./zh-tw": 328,
	"./zh-tw.js": 328
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 464;

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProjectPage = /** @class */ (function () {
    function ProjectPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ProjectPage.prototype.ngOnInit = function () {
        this.line1 = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.lineCanvas1.nativeElement, {
            type: 'line',
            data: {
                labels: [2015, 2016, 2017, 2018, 2019, 2020],
                datasets: [
                    {
                        label: "MMES Enrollment",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 5,
                        pointHitRadius: 10,
                        data: [11223, 16834, 20201, 29057, 32913, 46148],
                    }
                ]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: 'white'
                    },
                    position: 'bottom',
                },
                scales: {
                    xAxes: [{
                            ticks: {
                                fontColor: "#fff",
                            }
                        },],
                    yAxes: [{
                            ticks: {
                                fontColor: "#fff",
                            }
                        }]
                }
            }
        });
        this.bar = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barCanvas.nativeElement, {
            type: 'horizontalBar',
            data: {
                labels: [
                    "Others",
                    "OBC",
                    "SC",
                    "ST",
                ],
                datasets: [{
                        label: "Employement",
                        barPercentage: 0.5,
                        data: [78, 90, 60, 20],
                        backgroundColor: ["#00d4ff", "#8934df", "#ffab00", "#fc2e90"],
                    }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: 'white'
                    },
                    position: 'bottom',
                },
                cutoutPercentage: 90,
                scales: {
                    xAxes: [{
                            ticks: {
                                min: 10,
                                fontColor: "#fff",
                            }
                        },],
                    yAxes: [{
                            stacked: true,
                            ticks: {
                                fontColor: "#fff",
                            }
                        }]
                }
            }
        });
    };
    ProjectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProjectPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("lineCanvas1"),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]) === "function" && _a || Object)
    ], ProjectPage.prototype, "lineCanvas1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("barCanvas"),
        __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]) === "function" && _b || Object)
    ], ProjectPage.prototype, "barCanvas", void 0);
    ProjectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-project',template:/*ion-inline-start:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\project\project.html"*/'<!--\n  Generated template for the ProjectPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header >\n  <ion-navbar color="darkmode">   \n          <ion-title padding-left>MSME Community</ion-title>\n  </ion-navbar> \n</ion-header>\n<ion-content padding>  \n    <ion-item color="darkmode">\n        <ion-label>State</ion-label>\n        <ion-select [(ngModel)]="state">\n          <ion-option value="TN">TamilNadu</ion-option>\n          <ion-option value="kerala">Kerala</ion-option>\n          <ion-option value="AP">Andhra Pradesh</ion-option>\n          <ion-option value="Karnataka">Karnataka</ion-option>\n          <ion-option value="Mum">Mumbai</ion-option>\n          <ion-option value="UP">Uttar Pradesh</ion-option>\n          <ion-option value="MP">Madhya Pradesh</ion-option>\n        </ion-select>\n      </ion-item>\n<ion-grid>\n  <ion-row>\n    <ion-col>\n        <ion-card>\n          <ion-card-header text-center>Total MSMEs</ion-card-header>\n          <ion-card-content text-center>\n              112,227\n          </ion-card-content>\n        </ion-card>\n    </ion-col>\n    <ion-col>\n        <ion-card>\n            <ion-card-header text-center>Total Employment </ion-card-header>\n          <ion-card-content text-center>\n              621,423\n          </ion-card-content>\n        </ion-card>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n<img src="../../assets/imgs/doughNew.png">\n\n<canvas #lineCanvas1></canvas>\n<canvas #barCanvas></canvas>\n\n       \n      \n        \n  \n\n\n\n\n    <!-- <ion-item color="darkmode">\n        <ion-label>State</ion-label>\n        <ion-select [(ngModel)]="state">\n          <ion-option value="TN">TamilNadu</ion-option>\n          <ion-option value="kerala">Kerala</ion-option>\n          <ion-option value="AP">Andhra Pradesh</ion-option>\n          <ion-option value="Karnataka">Karnataka</ion-option>\n          <ion-option value="Mum">Mumbai</ion-option>\n          <ion-option value="UP">Uttar Pradesh</ion-option>\n          <ion-option value="MP">Madhya Pradesh</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item color="darkmode">\n          <ion-label>District</ion-label>\n          <ion-select [(ngModel)]="district">\n            <ion-option value="ch">Chennai</ion-option>\n            <ion-option value="cmbt">Coimbatore</ion-option>\n            <ion-option value="vlr">Vellore</ion-option>\n            <ion-option value="tvnm">Thiruvannamalai</ion-option>\n            <ion-option value="mad">Madurai</ion-option>\n            <ion-option value="tnvl">Thirunelveli</ion-option>\n            <ion-option value="kkmr">Kanyakumari</ion-option>\n          </ion-select>\n        </ion-item> -->\n    \n     \n   <ion-grid>\n     <!--<ion-row>\n      <ion-col col-6>\n          <select placeholder="Select State">\n            <option value="TN">TamilNadu</option>\n            <option value="kerala">Kerala</option>\n            <option value="AP">Andhra Pradesh</option>\n            <option value="Karnataka">Karnataka</option>\n            <option value="Mum">Mumbai</option>\n            <option value="UP">Uttar Pradesh</option>\n            <option value="MP">Madhya Pradesh</option>\n          </select>\n      </ion-col>\n      <ion-col col-6>            \n            <select placeholder="Select City">\n              <option value="ch">Chennai</option>\n              <option value="cmbt">Coimbatore</option>\n              <option value="vlr">Vellore</option>\n              <option value="tvnm">Thiruvannamalai</option>\n              <option value="mad">Madurai</option>\n              <option value="tnvl">Thirunelveli</option>\n              <option value="kkmr">Kanyakumari</option>\n            </select>\n        </ion-col>\n    </ion-row>  -->\n  </ion-grid>\n  <!-- <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n\n      </ion-col>\n      <ion-col col-6 class="msmeDetails">\n        <h4>Total MSMEs</h4>\n        <h5>240</h5>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-6>\n  \n        </ion-col>\n        <ion-col col-6 class="msmeDetails">\n          <h4>Total Revenue </h4>\n          <h5>5Cr.</h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-6>\n    \n          </ion-col>\n          <ion-col col-6 class="msmeDetails">\n            <h4>Revenue from SC/ST</h4>\n            <h5>2Cr.</h5>\n          </ion-col>\n        </ion-row>\n  </ion-grid> -->\n\n  <ion-fab right bottom >\n      <button ion-fab color="royal"><ion-icon class="fa fa-comments"></ion-icon></button>\n    </ion-fab>\n</ion-content>\n'/*ion-inline-end:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\project\project.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]) === "function" && _d || Object])
    ], ProjectPage);
    return ProjectPage;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=project.js.map

/***/ }),

/***/ 530:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_dash_board_dash_board__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_popover_popover__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, popoverCtrl, events) {
        var _this = this;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.footerIsHidden = false;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_dash_board_dash_board__["a" /* DashBoardPage */];
        events.subscribe('hideHeader', function (data) {
            _this.footerIsHidden = data.isHidden;
        });
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        //firebase.initializeApp(firebaseConfig);
    }
    MyApp.prototype.presentPopover = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_popover_popover__["a" /* PopoverComponent */]);
        popover.present({
            ev: myEvent
        });
    };
    MyApp.prototype.openFiles = function (page) {
        // this.nav.push(page);
        if (page === __WEBPACK_IMPORTED_MODULE_4__pages_dash_board_dash_board__["a" /* DashBoardPage */])
            this.nav.setRoot(page);
        else
            this.nav.push(page);
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component === __WEBPACK_IMPORTED_MODULE_4__pages_dash_board_dash_board__["a" /* DashBoardPage */])
            this.nav.setRoot(page.component);
        else
            this.nav.push(page.component);
    };
    MyApp.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashBoardPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]) === "function" && _a || Object)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\MSME-Conf\MSME\MSME-Prototype\src\app\app.html"*/'<ion-menu [content]="mycontent" class="menu">\n  \n \n  </ion-menu>  \n  \n<ion-nav #mycontent [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"D:\MSME-Conf\MSME\MSME-Prototype\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]) === "function" && _f || Object])
    ], MyApp);
    return MyApp;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashBoardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__project_project__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__remote_work_remote_work__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__grievance_grievance__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__training_training__ = __webpack_require__(128);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the DashBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashBoardPage = /** @class */ (function () {
    function DashBoardPage(menu, navCtrl, navParams) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toggleNotify = true;
        menu.enable(true);
        this.cardMenus = [
            {
                title: "MSME Community",
                icon: "fa fa-users",
                color: "messenger",
                Component: __WEBPACK_IMPORTED_MODULE_2__project_project__["a" /* ProjectPage */]
            },
            {
                title: "For your action",
                icon: "fa fa-pencil-square-o",
                color: "home",
                Component: __WEBPACK_IMPORTED_MODULE_3__remote_work_remote_work__["a" /* RemoteWorkPage */]
            },
            {
                title: "Grievances",
                icon: "fa fa-list-alt",
                color: "profile",
                Component: __WEBPACK_IMPORTED_MODULE_4__grievance_grievance__["a" /* GrievancePage */]
            },
            {
                title: "Training",
                icon: "fa fa-calendar-check-o",
                color: "loa",
                Component: __WEBPACK_IMPORTED_MODULE_5__training_training__["a" /* TrainingPage */]
            },
            {
                title: "Ledger",
                icon: "fa fa-book",
                color: "project"
            },
            {
                title: "Chat",
                icon: "fa fa-comments",
                color: "contacts",
            }
        ];
    }
    DashBoardPage.prototype.navigateComponent = function (page) {
        if (page.Component)
            this.navCtrl.push(page.Component);
    };
    DashBoardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashBoardPage');
    };
    DashBoardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dash-board',template:/*ion-inline-start:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\dash-board\dash-board.html"*/'<!--\n  Generated template for the DashBoardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="darkmode" hideBackButton>\n      <button ion-button icon-only menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>Dashboard</ion-title> \n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>    \n  <ion-grid>\n    <ion-row>\n      <ion-col col-6  *ngFor="let menu of cardMenus; let i=index" (click)="navigateComponent(menu)">\n          <ion-card text-center>    \n              <ion-card-content>\n                <ion-card-title >                   \n                    <button ion-button large clear [color]="menu.color">\n                        <ion-icon class="{{menu.icon}} cardFont"  ></ion-icon>\n                    </button>\n                    <ion-item class="dashLabel">\n                        <ion-label>{{menu.title}}</ion-label>\n                    </ion-item>\n                </ion-card-title>               \n              </ion-card-content> \n            </ion-card>\n      </ion-col>      \n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\dash-board\dash-board.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]) === "function" && _c || Object])
    ], DashBoardPage);
    return DashBoardPage;
    var _a, _b, _c;
}());

//# sourceMappingURL=dash-board.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RemoteWorkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RemoteWorkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RemoteWorkPage = /** @class */ (function () {
    function RemoteWorkPage(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
    }
    RemoteWorkPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RemoteWorkPage');
    };
    RemoteWorkPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-remote-work',template:/*ion-inline-start:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\remote-work\remote-work.html"*/'<!--\n  Generated template for the RemoteWorkPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="darkmode">\n    <ion-title padding-left>For your action</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-content>\n        Sales tax return filing date filing date extended\n    </ion-card-content>\n  </ion-card>\n  <ion-card>\n      <ion-card-content>\n          GST rules updated\n      </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <ion-card-content>\n    Cancellation of application is not possible once the payment has been made. No refunds will be given except in the event of cancellation or non- performance of service by www.msmeregistration.org.\n    GST rules updated\n        </ion-card-content>\n      </ion-card>\n    <ion-fab right bottom >\n        <button ion-fab color="royal"><ion-icon class="fa fa-comments"></ion-icon></button>\n      </ion-fab>\n</ion-content>\n'/*ion-inline-end:"D:\MSME-Conf\MSME\MSME-Prototype\src\pages\remote-work\remote-work.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], RemoteWorkPage);
    return RemoteWorkPage;
}());

//# sourceMappingURL=remote-work.js.map

/***/ })

},[379]);
//# sourceMappingURL=main.js.map