import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { EmojiPickerModule } from 'ionic-emoji-picker';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { FilePath } from '@ionic-native/file-path';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';

import { MyApp } from './app.component';
import { DashBoardPage } from '../pages/dash-board/dash-board';
import { ProjectPage } from '../pages/project/project';
import { RemoteWorkPage } from '../pages/remote-work/remote-work';
import { GrievancePage} from '../pages/grievance/grievance';  
import { TrainingPage} from '../pages/training/training';  
import { PopoverComponent } from '../components/popover/popover';

@NgModule({
  declarations: [
    MyApp,
    DashBoardPage,
    ProjectPage,
    RemoteWorkPage,
    PopoverComponent,
    GrievancePage,
    TrainingPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    EmojiPickerModule.forRoot(),
    IonicModule.forRoot(MyApp), 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashBoardPage,
    ProjectPage,
    RemoteWorkPage,
    PopoverComponent,
    GrievancePage,
    TrainingPage
  ],
  providers: [
    // ImagePicker,
    //  Crop,  
    File,  
    Camera,
    StatusBar,
    SplashScreen,
    FileChooser,
    FileOpener,
    FilePath,    
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
