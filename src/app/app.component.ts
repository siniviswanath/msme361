import { Component,ViewChild } from '@angular/core';
import { Platform, Nav,PopoverController,Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProjectPage } from '../pages/project/project';
import { DashBoardPage } from '../pages/dash-board/dash-board';
import { PopoverComponent } from '../components/popover/popover';
import { RemoteWorkPage} from '../pages/remote-work/remote-work';  
import { GrievancePage} from '../pages/grievance/grievance';  
import { TrainingPage} from '../pages/training/training';  
export interface MenuItem {
  title: string;
  component: any;
  icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public footerIsHidden: boolean = false;
  rootPage:any = DashBoardPage;
  appMenuItems: Array<MenuItem>;
  public footerButtons:any;
  constructor(    platform: Platform,
                  statusBar: StatusBar, 
                  splashScreen: SplashScreen,
                  public popoverCtrl: PopoverController,
                  public events: Events ) {
      
      events.subscribe('hideHeader', (data) => {
        this.footerIsHidden = data.isHidden;
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    //firebase.initializeApp(firebaseConfig);
 
   
  }
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
  }
  openFiles(page){
    // this.nav.push(page);
    if(page===DashBoardPage)
    this.nav.setRoot(page);
    else      
    this.nav.push(page);
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.component===DashBoardPage)
    this.nav.setRoot(page.component);
    else      
    this.nav.push(page.component);
  }

  ionViewDidLoad() {  
    console.log('ionViewDidLoad DashBoardPage');
  }
}

