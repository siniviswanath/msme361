import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
/**
 * Generated class for the ProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project',
  templateUrl: 'project.html',
})
export class ProjectPage implements OnInit {
  
  @ViewChild("lineCanvas1") lineCanvas1: ElementRef;
  @ViewChild("barCanvas") barCanvas: ElementRef;
  
  public line1: Chart;
  public bar: Chart;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,

  ) {  
   
  }

 

  ngOnInit() {
    this.line1 = new Chart(this.lineCanvas1.nativeElement, {
      type: 'line',
       data: {
        labels: [2015, 2016,2017,2018,2019,2020],
        datasets: [
            {
                label: "MMES Enrollment",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 10,
                data: [11223, 16834, 20201, 29057, 32913, 46148],
            }
        ]
    },
   
    options: {
      legend: {
        labels: {
            fontColor: 'white'
        },            
        position:'bottom',
      },
      scales: {
        xAxes: [{
            ticks: {
            fontColor: "#fff", 
            }
        },],
        yAxes: [{
          ticks: {
            fontColor: "#fff", 
            }
        }]
    }
  }
  });
  this.bar = new Chart(this.barCanvas.nativeElement, {
    type: 'horizontalBar',
    data:{
      labels: [
          "Others",
          "OBC",
          "SC",
          "ST",
      ],
      datasets: [{
        label: "Employement",
        barPercentage: 0.5,
          data: [78, 90,60,20],
          backgroundColor: [ "#00d4ff","#8934df","#ffab00","#fc2e90"],
          //hoverBackgroundColor: ["#66A2EB", "#FCCE56"]
      }]
  },
    options: {
      legend: {
        labels: {
            fontColor: 'white'
        },            
        position:'bottom',
      },
      cutoutPercentage: 90,
        scales: {
            xAxes: [{
                ticks: {
                min: 10,
                fontColor: "#fff", 
                }
            },],
            yAxes: [{
              stacked: true,
              ticks: {
                fontColor: "#fff", 
                }
            }]
        }

    }
});
  }
 


  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectPage');
  }

}
