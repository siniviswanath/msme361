import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the RemoteWorkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-remote-work',
  templateUrl: 'remote-work.html',
})
export class RemoteWorkPage {
 
  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder) {
  

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RemoteWorkPage');
  }

}
