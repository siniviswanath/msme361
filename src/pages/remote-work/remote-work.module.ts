import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemoteWorkPage } from './remote-work';

@NgModule({
  declarations: [
    RemoteWorkPage,
  ],
  imports: [
    IonicPageModule.forChild(RemoteWorkPage),
  ],
})
export class RemoteWorkPageModule {}
