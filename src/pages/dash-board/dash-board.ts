import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController } from 'ionic-angular';

import {ProjectPage} from '../project/project';
import {RemoteWorkPage} from '../remote-work/remote-work';
import { GrievancePage} from '../grievance/grievance';  
import { TrainingPage} from '../training/training';  
/**
 * Generated class for the DashBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dash-board',
  templateUrl: 'dash-board.html',
})
export class DashBoardPage {
public cardMenus:any;
public recent:any;
public footerButtons:any;
toggleNotify:Boolean=true;

  constructor(public menu: MenuController,public navCtrl: NavController, public navParams: NavParams) {
    menu.enable(true);
    this.cardMenus=[
      {
        title:"MSME Community",
        icon:"fa fa-users",
        color:"messenger",
        Component:ProjectPage
      },
      {
        title:"For your action",
        icon:"fa fa-pencil-square-o",
        color:"home",
        Component:RemoteWorkPage
      },
      {
        title:"Grievances",
        icon:"fa fa-list-alt",
        color:"profile",
        Component:GrievancePage
      },
      {
        title:"Training",
        icon:"fa fa-calendar-check-o",
        color:"loa",
        Component:TrainingPage
      
      },     
      {
        title:"Ledger",
        icon:"fa fa-book",
        color:"project"
      },
      {
        title:"Chat",
        icon:"fa fa-comments",
        color:"contacts",
      }      
    ];
    

  }

navigateComponent(page){
  if(page.Component)
  this.navCtrl.push(page.Component);
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad DashBoardPage');
  }

}
