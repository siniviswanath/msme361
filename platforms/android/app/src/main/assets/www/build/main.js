webpackJsonp([8],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var PopoverComponent = /** @class */ (function () {
    function PopoverComponent() {
        this.popoverItemChat = [];
        this.popoverItemChat = [
            { itemName: 'View Profile' },
            { itemName: 'Edit Profile' }
        ];
    }
    PopoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'popover',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\components\popover\popover.html"*/'<!-- Generated template for the PopoverComponent component -->\n<ion-list  *ngFor="let item of popoverItemChat" no-padding>\n    <ion-item no-lines >\n        {{item.itemName}}\n      </ion-item>\n</ion-list>\n\n\n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\components\popover\popover.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], PopoverComponent);
    return PopoverComponent;
}());

//# sourceMappingURL=popover.js.map

/***/ }),

/***/ 139:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 139;

/***/ }),

/***/ 180:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/chat/chat.module": [
		529,
		7
	],
	"../pages/dash-board/dash-board.module": [
		530,
		6
	],
	"../pages/files/files.module": [
		531,
		5
	],
	"../pages/group-chat/group-chat.module": [
		532,
		4
	],
	"../pages/leave/leave.module": [
		533,
		3
	],
	"../pages/modal/modal.module": [
		534,
		0
	],
	"../pages/project/project.module": [
		536,
		2
	],
	"../pages/users/users.module": [
		535,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 180;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dash_board_dash_board__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__project_project__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, 
        //private nativePageTransitions: NativePageTransitions,
        menu, events) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.events = events;
        this.testUsername = "Elan";
        this.testPassword = "Elan";
        this.username = "";
        this.password = "";
        events.publish('hideHeader', { isHidden: true });
        this.testUnPwd = [
            { username: 'Ramesh', password: 'ramesh' },
            { username: 'Elan', password: 'Elan' },
            { username: 'Vicky', password: 'Vicky' },
            { username: 'Eliyas', password: 'Eliyas' }
        ];
        menu.enable(false);
    }
    HomePage.prototype.ionViewWillLeave = function () {
        //Make footer visiable while leaving the page.
        this.events.publish('hideHeader', { isHidden: false });
    };
    HomePage.prototype.project = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__project_project__["a" /* ProjectPage */]);
    };
    HomePage.prototype.login = function (un, pwd) {
        this.username = un;
        this.password = pwd;
        console.log(this.username);
        // let options: NativeTransitionOptions = {
        //   direction: 'left',
        //   duration: 500,
        //   // slowdownfactor: 3,
        //   // slidePixels: 20,
        //   // iosdelay: 100,
        //   // androiddelay: 150,
        //   // fixedPixelsTop: 0,
        //   // fixedPixelsBottom: 60
        //  };
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dash_board_dash_board__["a" /* DashBoardPage */], { username: this.username });
        //  for(let un in this.testUnPwd){
        //    if(this.testUnPwd[un].username==this.username && this.testUnPwd[un].password== this.password){
        //     this.nativePageTransitions.slide(options);
        //     this.navCtrl.setRoot(DashBoardPage,{username:this.username});
        //    }
        //  }
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\home\home.html"*/'<div class="bgImg" >\n  </div>    \n<ion-content text-center padding class="animated scrollcontent" > \n        <!-- <img src="assets/imgs/splash.png" class="logo"> -->\n    <!-- class="animated scrollcontent" -->\n    <ion-card class="ionCard">\n        <ion-input placeholder="Username"  type="text" [(ngModel)]="username" ></ion-input> \n        <ion-input placeholder="Password" type="password" [(ngModel)]="password"></ion-input>\n    </ion-card>\n    \n    <!-- <img src="assets/imgs/splash.png" class="logo">\n    <ion-list>\n        <ion-item text-center no-lines>\n            <ion-icon name="contact"  class="userIcon" color="CNSI"></ion-icon>\n          </ion-item>  \n        <ion-item >\n          <ion-label fixed>Username</ion-label>\n          <ion-input placeholder="Username"  type="text" [(ngModel)]="username"></ion-input>     \n        </ion-item>      \n        <ion-item>\n          <ion-label fixed>Password</ion-label>\n          <ion-input placeholder="Password" type="password" [(ngModel)]="password"></ion-input>   \n        </ion-item>      \n      </ion-list>-->\n<button ion-button round (click)="login(username,password)" color="balanced" class="loginBtn">Login</button> \n</ion-content>\n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(399);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_emoji_picker__ = __webpack_require__(480);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(526);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_chooser__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_opener__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_path__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_storage__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_native_page_transitions__ = __webpack_require__(527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__app_component__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_home_home__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_dash_board_dash_board__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_users_users__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_chat_chat__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_project_project__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_group_chat_group_chat__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_files_files__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_leave_leave__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ng_socket_io__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_23_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_popover_popover__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














//import { Crop } from '@ionic-native/crop';
//import { ImagePicker } from '@ionic-native/image-picker';











var config = { url: 'http://192.168.43.174:3001', options: {} };
//const config: SocketIoConfig = { url: 'http://10.86.9.161:3001', options: {} };
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_dash_board_dash_board__["a" /* DashBoardPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_users_users__["a" /* UsersPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_project_project__["a" /* ProjectPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_group_chat_group_chat__["a" /* GroupChatPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_files_files__["a" /* FilesPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_leave_leave__["a" /* LeavePage */],
                __WEBPACK_IMPORTED_MODULE_24__components_popover_popover__["a" /* PopoverComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_23_ng_socket_io__["SocketIoModule"].forRoot(config),
                __WEBPACK_IMPORTED_MODULE_6_ionic_emoji_picker__["a" /* EmojiPickerModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dash-board/dash-board.module#DashBoardPageModule', name: 'DashBoardPage', segment: 'dash-board', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/files/files.module#FilesPageModule', name: 'FilesPage', segment: 'files', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/group-chat/group-chat.module#GroupChatPageModule', name: 'GroupChatPage', segment: 'group-chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/leave/leave.module#LeavePageModule', name: 'LeavePage', segment: 'leave', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal/modal.module#ModalPageModule', name: 'ModalPage', segment: 'modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/users/users.module#UsersPageModule', name: 'UsersPage', segment: 'users', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/project/project.module#ProjectPageModule', name: 'ProjectPage', segment: 'project', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_12__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_dash_board_dash_board__["a" /* DashBoardPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_users_users__["a" /* UsersPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_project_project__["a" /* ProjectPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_group_chat_group_chat__["a" /* GroupChatPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_files_files__["a" /* FilesPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_leave_leave__["a" /* LeavePage */],
                __WEBPACK_IMPORTED_MODULE_24__components_popover_popover__["a" /* PopoverComponent */]
            ],
            providers: [
                // ImagePicker,
                //  Crop,  
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 445:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 462:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 201,
	"./af.js": 201,
	"./ar": 202,
	"./ar-dz": 203,
	"./ar-dz.js": 203,
	"./ar-kw": 204,
	"./ar-kw.js": 204,
	"./ar-ly": 205,
	"./ar-ly.js": 205,
	"./ar-ma": 206,
	"./ar-ma.js": 206,
	"./ar-sa": 207,
	"./ar-sa.js": 207,
	"./ar-tn": 208,
	"./ar-tn.js": 208,
	"./ar.js": 202,
	"./az": 209,
	"./az.js": 209,
	"./be": 210,
	"./be.js": 210,
	"./bg": 211,
	"./bg.js": 211,
	"./bm": 212,
	"./bm.js": 212,
	"./bn": 213,
	"./bn.js": 213,
	"./bo": 214,
	"./bo.js": 214,
	"./br": 215,
	"./br.js": 215,
	"./bs": 216,
	"./bs.js": 216,
	"./ca": 217,
	"./ca.js": 217,
	"./cs": 218,
	"./cs.js": 218,
	"./cv": 219,
	"./cv.js": 219,
	"./cy": 220,
	"./cy.js": 220,
	"./da": 221,
	"./da.js": 221,
	"./de": 222,
	"./de-at": 223,
	"./de-at.js": 223,
	"./de-ch": 224,
	"./de-ch.js": 224,
	"./de.js": 222,
	"./dv": 225,
	"./dv.js": 225,
	"./el": 226,
	"./el.js": 226,
	"./en-SG": 227,
	"./en-SG.js": 227,
	"./en-au": 228,
	"./en-au.js": 228,
	"./en-ca": 229,
	"./en-ca.js": 229,
	"./en-gb": 230,
	"./en-gb.js": 230,
	"./en-ie": 231,
	"./en-ie.js": 231,
	"./en-il": 232,
	"./en-il.js": 232,
	"./en-nz": 233,
	"./en-nz.js": 233,
	"./eo": 234,
	"./eo.js": 234,
	"./es": 235,
	"./es-do": 236,
	"./es-do.js": 236,
	"./es-us": 237,
	"./es-us.js": 237,
	"./es.js": 235,
	"./et": 238,
	"./et.js": 238,
	"./eu": 239,
	"./eu.js": 239,
	"./fa": 240,
	"./fa.js": 240,
	"./fi": 241,
	"./fi.js": 241,
	"./fo": 242,
	"./fo.js": 242,
	"./fr": 243,
	"./fr-ca": 244,
	"./fr-ca.js": 244,
	"./fr-ch": 245,
	"./fr-ch.js": 245,
	"./fr.js": 243,
	"./fy": 246,
	"./fy.js": 246,
	"./ga": 247,
	"./ga.js": 247,
	"./gd": 248,
	"./gd.js": 248,
	"./gl": 249,
	"./gl.js": 249,
	"./gom-latn": 250,
	"./gom-latn.js": 250,
	"./gu": 251,
	"./gu.js": 251,
	"./he": 252,
	"./he.js": 252,
	"./hi": 253,
	"./hi.js": 253,
	"./hr": 254,
	"./hr.js": 254,
	"./hu": 255,
	"./hu.js": 255,
	"./hy-am": 256,
	"./hy-am.js": 256,
	"./id": 257,
	"./id.js": 257,
	"./is": 258,
	"./is.js": 258,
	"./it": 259,
	"./it-ch": 260,
	"./it-ch.js": 260,
	"./it.js": 259,
	"./ja": 261,
	"./ja.js": 261,
	"./jv": 262,
	"./jv.js": 262,
	"./ka": 263,
	"./ka.js": 263,
	"./kk": 264,
	"./kk.js": 264,
	"./km": 265,
	"./km.js": 265,
	"./kn": 266,
	"./kn.js": 266,
	"./ko": 267,
	"./ko.js": 267,
	"./ku": 268,
	"./ku.js": 268,
	"./ky": 269,
	"./ky.js": 269,
	"./lb": 270,
	"./lb.js": 270,
	"./lo": 271,
	"./lo.js": 271,
	"./lt": 272,
	"./lt.js": 272,
	"./lv": 273,
	"./lv.js": 273,
	"./me": 274,
	"./me.js": 274,
	"./mi": 275,
	"./mi.js": 275,
	"./mk": 276,
	"./mk.js": 276,
	"./ml": 277,
	"./ml.js": 277,
	"./mn": 278,
	"./mn.js": 278,
	"./mr": 279,
	"./mr.js": 279,
	"./ms": 280,
	"./ms-my": 281,
	"./ms-my.js": 281,
	"./ms.js": 280,
	"./mt": 282,
	"./mt.js": 282,
	"./my": 283,
	"./my.js": 283,
	"./nb": 284,
	"./nb.js": 284,
	"./ne": 285,
	"./ne.js": 285,
	"./nl": 286,
	"./nl-be": 287,
	"./nl-be.js": 287,
	"./nl.js": 286,
	"./nn": 288,
	"./nn.js": 288,
	"./pa-in": 289,
	"./pa-in.js": 289,
	"./pl": 290,
	"./pl.js": 290,
	"./pt": 291,
	"./pt-br": 292,
	"./pt-br.js": 292,
	"./pt.js": 291,
	"./ro": 293,
	"./ro.js": 293,
	"./ru": 294,
	"./ru.js": 294,
	"./sd": 295,
	"./sd.js": 295,
	"./se": 296,
	"./se.js": 296,
	"./si": 297,
	"./si.js": 297,
	"./sk": 298,
	"./sk.js": 298,
	"./sl": 299,
	"./sl.js": 299,
	"./sq": 300,
	"./sq.js": 300,
	"./sr": 301,
	"./sr-cyrl": 302,
	"./sr-cyrl.js": 302,
	"./sr.js": 301,
	"./ss": 303,
	"./ss.js": 303,
	"./sv": 304,
	"./sv.js": 304,
	"./sw": 305,
	"./sw.js": 305,
	"./ta": 306,
	"./ta.js": 306,
	"./te": 307,
	"./te.js": 307,
	"./tet": 308,
	"./tet.js": 308,
	"./tg": 309,
	"./tg.js": 309,
	"./th": 310,
	"./th.js": 310,
	"./tl-ph": 311,
	"./tl-ph.js": 311,
	"./tlh": 312,
	"./tlh.js": 312,
	"./tr": 313,
	"./tr.js": 313,
	"./tzl": 314,
	"./tzl.js": 314,
	"./tzm": 315,
	"./tzm-latn": 316,
	"./tzm-latn.js": 316,
	"./tzm.js": 315,
	"./ug-cn": 317,
	"./ug-cn.js": 317,
	"./uk": 318,
	"./uk.js": 318,
	"./ur": 319,
	"./ur.js": 319,
	"./uz": 320,
	"./uz-latn": 321,
	"./uz-latn.js": 321,
	"./uz.js": 320,
	"./vi": 322,
	"./vi.js": 322,
	"./x-pseudo": 323,
	"./x-pseudo.js": 323,
	"./yo": 324,
	"./yo.js": 324,
	"./zh-cn": 325,
	"./zh-cn.js": 325,
	"./zh-hk": 326,
	"./zh-hk.js": 326,
	"./zh-tw": 327,
	"./zh-tw.js": 327
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 462;

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng_socket_io__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_chooser__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_opener__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_popover_popover__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common_http__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatPage = /** @class */ (function () {
    function ChatPage(navCtrl, navParams, socket, toastCtrl, camera, filechooser, filepath, fileopener, popoverCtrl, storage, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.socket = socket;
        this.toastCtrl = toastCtrl;
        this.camera = camera;
        this.filechooser = filechooser;
        this.filepath = filepath;
        this.fileopener = fileopener;
        this.popoverCtrl = popoverCtrl;
        this.storage = storage;
        this.http = http;
        this.items = [];
        this.key = 'item';
        this.messages = [];
        this.nickname = '';
        this.imgPath = '';
        this.message = '';
        this.toggled = false;
        this.myObj = {
            "ProjName": "",
            "Logo": "",
            "ProjResources": "",
        };
        this.nickname = this.navParams.get('nickname');
        this.imgPath = this.navParams.get('imgPath');
        this.UserItemClicked = this.navParams.get('UserItem');
        //console.log(this.UserItemClicked.username);
        this.getMessages().subscribe(function (message) {
            _this.messages.push(message);
        });
    }
    ChatPage.prototype.getDataformjson = function () {
        var _this = this;
        var data = this.http.get('../../assets/data/userList.json');
        //https://jsonplaceholder.typicode.com/posts
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    ChatPage.prototype.saveData = function () {
        this.myObj.ProjName = "New Project";
        this.items.push(this.myObj);
        console.log(this.items);
        this.storage.set(this.key, JSON.stringify(this.items));
    };
    ChatPage.prototype.loadData = function () {
        var _this = this;
        this.storage.get(this.key).then(function (val) {
            if (val != null && val != undefined)
                _this.items = JSON.parse(val);
        });
    };
    ChatPage.prototype.presentPopover = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_8__components_popover_popover__["a" /* PopoverComponent */]);
        popover.present({
            ev: myEvent
        });
    };
    ChatPage.prototype.attach = function () {
        var _this = this;
        this.filechooser.open().then(function (file) {
            _this.filepath.resolveNativePath(file).then(function (resolvedfilepath) {
                _this.fileopener.open(resolvedfilepath, 'application/pdf').then(function (value) {
                    alert('It worked');
                }).catch(function (err) {
                    alert(JSON.stringify(err));
                });
            }).catch(function (err) {
                alert(JSON.stringify(err));
            });
        }).catch(function (err) {
            alert(JSON.stringify(err));
        });
    };
    ChatPage.prototype.snap = function () {
        var _this = this;
        var options = {
            quality: 70,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            _this.pic = 'data:image/jpeg;base64,' + imageData;
            alert(_this.pic);
            _this.myObj.Logo = _this.pic;
            _this.items.push(_this.myObj);
            //this.message += this.pic;
        }, function (err) {
            alert(err);
            // Handle error
        });
    };
    ChatPage.prototype.handleSelection = function (event) {
        this.message += event.char;
    };
    ChatPage.prototype.sendMessage = function () {
        this.socket.emit('add-message', { text: this.message });
        console.log(this.message);
        this.typedMessage = this.message;
        console.log(this.typedMessage);
        console.log("Send Message " + this.message);
        this.message = '';
    };
    ChatPage.prototype.getMessages = function () {
        var _this = this;
        var observable = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"](function (observer) {
            _this.socket.on('message', function (data) {
                observer.next(data);
                console.log(data);
            });
        });
        return observable;
    };
    ChatPage.prototype.getUsers = function () {
        var _this = this;
        var observable = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"](function (observer) {
            _this.socket.on('users-changed', function (data) {
                observer.next(data);
            });
        });
        return observable;
    };
    ChatPage.prototype.ionViewWillLeave = function () {
        this.socket.disconnect();
    };
    ChatPage.prototype.showToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    };
    ChatPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatPage');
    };
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\chat\chat.html"*/'<ion-header>\n  <ion-navbar color="balanced">\n      <ion-title>\n          <ion-grid>\n              <ion-row>              \n                <ion-col col-2>\n                    <ion-avatar>\n                        <img [src]="imgPath"  class="ion-avatarClass">\n                    </ion-avatar>           \n                </ion-col>          \n                <ion-col col-6 >\n                  <label class="text">{{nickname}}</label><br>\n                  <label class="lastSeen">last seen today at 10:41 AM</label>                    \n                </ion-col>\n                <ion-col col-4 no-padding class="optionButton">\n                    <button ion-button color="light" clear no-padding>  \n                        <ion-icon ios="ios-call" md="md-call" ></ion-icon>\n                    </button> \n                    <button ion-button color="light" clear  no-padding>\n                        <ion-icon ios="ios-videocam" md="md-videocam" ></ion-icon>\n                     </button> \n                     <button ion-button color="light" clear  no-padding (click)="presentPopover($event)">\n                        <ion-icon name="more" ></ion-icon>\n                      </button> \n                </ion-col>\n                \n                <!-- <ion-col col-2  class="textpadding">         \n                    <button ion-button color="danger" clear large>  \n                        <ion-icon ios="ios-call" md="md-call" ></ion-icon>\n                    </button> \n                </ion-col>\n                <ion-col col-2 class="textpadding">\n                    <button ion-button color="danger" clear large>\n                       <ion-icon ios="ios-videocam" md="md-videocam" ></ion-icon>\n                    </button>                   \n                </ion-col>\n                <button ion-button color="danger" clear large>\n                  <ion-icon name="more" ></ion-icon>\n                </button> -->\n              </ion-row>\n            </ion-grid>                         \n      </ion-title>\n    </ion-navbar>\n  </ion-header>\n  <ion-content class="chat">    \n          <!-- <ion-grid>\n                  <ion-row *ngFor="let message of messages">      \n                    <ion-col col-9 *ngIf="message.from !== nickname" class="message" [ngClass]="{\'my_message\': message.from === nickname, \'other_message\': message.from !== nickname}">\n                      <div class="user_name">{{ message.from }}</div>                         \n                      <span>{{ message.text }}</span>\n                      <span class="time">{{message.created | date:\'hh:MM:ss\'}}</span>\n                    </ion-col>\n                \n                    <ion-col offset-3 col-9 *ngIf="message.from === nickname" class="message" [ngClass]="{\'my_message\': message.from === nickname, \'other_message\': message.from !== nickname}">\n                      <div class="user_name">Me</div>\n                      <span>{{ message.text }}</span>                      \n                      <p><img src="{{pic}}"></p>\n                      <span class="time">{{message.created | date:\'hh:MM:ss\'}}</span>\n                    </ion-col>                  \n                  </ion-row>\n                </ion-grid> -->\n                <ion-grid>\n                  <ion-row>      \n                    <ion-col col-9  class="message">\n                      <div class="user_name">{{ message.from }}</div>                         \n                      <span>{{ message.text }}</span>\n                      <span class="time">{{message.created | date:\'hh:MM:ss\'}}</span>\n                    </ion-col>\n                \n                    <ion-col offset-3 col-9 class="message">\n                      <div class="user_name">Me</div>\n                      <span>{{ typedMessage }}</span>                      \n                       <p><img src="{{pic}}"></p> \n                      <!-- <span class="time">{{message.created | date:\'hh:MM:ss\'}}</span> -->\n                    </ion-col>                  \n                  </ion-row>\n                </ion-grid>\n      <ion-fab right bottom>\n          <button ion-fab color="royal"><ion-icon name="arrow-dropleft"></ion-icon></button>\n          <ion-fab-list side="left">\n            <button ion-fab (click)="attach()"><ion-icon name="attach"></ion-icon></button>\n            <button ion-fab (click)="snap()"><ion-icon name="camera"></ion-icon></button>\n            <button ion-fab icon-only (click)="toggled = !toggled" [(emojiPickerIf)]="toggled" [emojiPickerDirection]="\'top\'"\n            (emojiPickerSelect)="handleSelection($event)"><ion-icon name="happy"></ion-icon></button>\n          </ion-fab-list>\n        </ion-fab>\n        <button ion-button (click)=\'getDataformjson()\'> Get Data from Json</button>\n        <button ion-button (click)=\'saveData()\'>SaveData</button>\n        <button ion-button (click)=\'loadData()\'> LoadData</button>\n        <ion-list>\n          <ion-item *ngFor="let item of items">\n            {{item.Logo}}\n              <p><img src="{{item.Logo}}"></p> \n            <!-- {{item.Logo}} -->\n          </ion-item>\n        </ion-list>\n  </ion-content>   \n<ion-footer>\n    <ion-grid class="inputmessage">\n        <ion-row class="input-Class">\n          <ion-col col-11>\n              <ion-input type="text"  placeholder="Type a message" [(ngModel)]="message"></ion-input>\n          </ion-col>\n          <ion-col col-1 class="textpadding">                \n                <button class="sendBtn" [disabled]="message === \'\'" (click)="sendMessage()"><ion-icon name="send"  class="sendIcon"></ion-icon></button>                   \n          </ion-col>\n        </ion-row>\n      </ion-grid>\n</ion-footer>\n      \n    \n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\chat\chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ng_socket_io__["Socket"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_chooser__["a" /* FileChooser */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_opener__["a" /* FileOpener */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_10__angular_common_http__["a" /* HttpClient */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_chat__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__users_users__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__group_chat_group_chat__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProjectPage = /** @class */ (function () {
    //nameList:any[];
    function ProjectPage(menu, navCtrl, navParams, modalCtrl) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        //public status:boolean;
        this.enablesearch = false;
        this.myObj = {
            "ProjName": "",
            "Logo": "",
            "ProjResources": "",
        };
        this.project = [
            {
                "ProjName": "NHVRINplus",
                "Logo": "assets/imgs/NH Seal.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Swetha",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                        "username": "Lalli",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/baby.jpg",
                        "username": "Sowbhi",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Michael",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Divya",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Padhu",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/spiderman.jpg",
                        "username": "Gopi",
                        "msg": "Hello",
                        "status": false
                    }
                ]
            },
            {
                "ProjName": "COGNOS",
                "Logo": "assets/imgs/Cognos.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": false
                    },
                ]
            },
            {
                "ProjName": "NHVRINplus",
                "Logo": "assets/imgs/NH Seal.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Swetha",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                        "username": "Lalli",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/baby.jpg",
                        "username": "Sowbhi",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Michael",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Divya",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Padhu",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/spiderman.jpg",
                        "username": "Gopi",
                        "msg": "Hello",
                        "status": false
                    }
                ]
            },
            {
                "ProjName": "COGNOS",
                "Logo": "assets/imgs/Cognos.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": false
                    },
                ]
            },
            {
                "ProjName": "NHVRINplus",
                "Logo": "assets/imgs/NH Seal.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Swetha",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                        "username": "Lalli",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/baby.jpg",
                        "username": "Sowbhi",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Michael",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Divya",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Padhu",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/spiderman.jpg",
                        "username": "Gopi",
                        "msg": "Hello",
                        "status": false
                    }
                ]
            },
            {
                "ProjName": "COGNOS",
                "Logo": "assets/imgs/Cognos.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": false
                    },
                ]
            },
            {
                "ProjName": "NHVRINplus",
                "Logo": "assets/imgs/NH Seal.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Swetha",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                        "username": "Lalli",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/baby.jpg",
                        "username": "Sowbhi",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Michael",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Divya",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Padhu",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/spiderman.jpg",
                        "username": "Gopi",
                        "msg": "Hello",
                        "status": false
                    }
                ]
            },
            {
                "ProjName": "COGNOS",
                "Logo": "assets/imgs/Cognos.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": false
                    },
                ]
            },
            {
                "ProjName": "COGNOS",
                "Logo": "assets/imgs/Cognos.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": false
                    },
                ]
            },
            {
                "ProjName": "COGNOS",
                "Logo": "assets/imgs/Cognos.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": false
                    },
                ]
            },
            {
                "ProjName": "COGNOS",
                "Logo": "assets/imgs/Cognos.png",
                "ProjResources": [
                    {
                        "imgSrc": "assets/imgs/bike.jpg",
                        "username": "Ramesh",
                        "msg": "Hello",
                        "status": false
                    },
                    {
                        "imgSrc": "assets/imgs/motivate.jpg",
                        "username": "Bala",
                        "msg": "Hello",
                        "status": true
                    },
                    {
                        "imgSrc": "assets/imgs/rose.jpg",
                        "username": "Prakash",
                        "msg": "Hello",
                        "status": false
                    },
                ]
            },
        ];
        this.nickname = this.navParams.get('username');
        menu.enable(true);
        //this.status=true;
    }
    ProjectPage.prototype.enableSearchBar = function () {
        console.log(this.enablesearch);
        this.enablesearch = !this.enablesearch;
    };
    ProjectPage.prototype.toggleSection = function (i) {
        this.project[i].open = !this.project[i].open;
    };
    ProjectPage.prototype.opengroupChat = function (un, img, names) {
        var nameList = [];
        for (var i in names) {
            nameList.push(names[i].username);
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__group_chat_group_chat__["a" /* GroupChatPage */], { nickname: un, imgPath: img, users: nameList });
    };
    ProjectPage.prototype.openChat = function (un, img) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__chat_chat__["a" /* ChatPage */], { nickname: un, imgPath: img });
    };
    ProjectPage.prototype.addProject = function () {
        this.project.push(this.myObj);
    };
    ProjectPage.prototype.deleteProject = function () {
        console.log(this.project);
        var length = this.project.length;
        this.project.splice(length - 1);
    };
    ProjectPage.prototype.openModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create('ModalPage', { Title: "Add Project" });
        modal.onDidDismiss(function (data) {
            _this.myObj.ProjName = data.ModalName;
            if (_this.myObj.ProjName != '')
                _this.addProject();
        });
        modal.present();
    };
    ProjectPage.prototype.openContacts = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__users_users__["a" /* UsersPage */], { username: this.nickname });
    };
    ProjectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProjectPage');
    };
    ProjectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-project',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\project\project.html"*/'<!--\n  Generated template for the ProjectPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header >\n  <ion-navbar color="balanced">   \n      <!-- <button ion-button icon-only menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>       -->\n          <ion-title padding-left>Messenger Forum</ion-title>\n          <ion-buttons end>             \n              <button ion-button class="headerButtonpadding">\n                  <ion-icon name="home" item-end class="homeBtn"></ion-icon>\n              </button>\n              <button ion-button (click)=\'enableSearchBar()\' >\n                  <ion-icon item-end  class="homeBtn" name="search"></ion-icon>\n              </button>\n              \n            </ion-buttons>          \n  </ion-navbar> \n</ion-header>\n<ion-content padding>\n    <ion-searchbar *ngIf="enablesearch" class="searchAnimate" [ngClass]="{\'searchExpand\':enablesearch,\'searchCollapse\':!enablesearch}"></ion-searchbar>\n    <!-- (ionInput)="getItems($event)" -->\n  <ion-list>    \n <ion-list>\n   <ion-item-sliding *ngFor="let item of project ; let i = index">\n     <ion-item no-lines>\n        <ion-grid no-padding  class="animated fadeInDown">\n            <ion-row no-padding>\n              <ion-col col-12>\n                 <ion-card>\n                     <ion-card-header ion-item (click)="toggleSection(i)">    \n                        <ion-icon name="arrow-dropright" *ngIf="!item.open" item-left></ion-icon>\n                       <ion-icon name="arrow-dropdown" *ngIf="item.open" item-left></ion-icon> \n                         <ion-avatar item-start>\n                             <img [src]="item.Logo">\n                           </ion-avatar>\n                       {{item.ProjName}}                      \n                       <!-- <button item-end ion-button color="orange" class="chatBtn" (click)="opengroupChat(item.ProjName,item.Logo,item.ProjResources)">\n                          <ion-icon name="chatboxes"></ion-icon>\n                        </button> -->\n                     </ion-card-header>                     \n                     <ion-card-content *ngIf="item.ProjResources && item.open" >\n                        <ion-list class="animated"  [ngClass]="{\'fadeInDown\':item.open,\'fadeOutUp\':!item.open}">\n                               <ion-item-sliding #item *ngFor="let res of item.ProjResources" >                                 \n                                  <ion-item *ngIf="res.username" (click)="openChat(res.username,res.imgSrc)" class="ion-list-border">\n                                      <ion-avatar item-start>\n                                          <img [src]="res.imgSrc">\n                                        </ion-avatar>\n                                        <label>\n                                          <h4>{{res.username}}</h4>\n                                            <p>{{res.msg}}</p> \n                                        </label>\n                                        <ion-note item-end [ngClass]="{\'online\':res.status,\'offline\':!res.status}"></ion-note>\n                                  </ion-item>\n                            <ion-item-options side="left">\n                              <button ion-button color="balanced" large class="ionButton" ><ion-icon ios="ios-call" md="md-call" ></ion-icon></button>\n                              <button ion-button large class="ionButton" ><ion-icon ios="ios-videocam" md="md-videocam" ></ion-icon></button>\n                            </ion-item-options> \n                            <ion-item-options side="right">                               \n                              <button ion-button color="assertive" large class="ionButton" ><ion-icon name="trash"></ion-icon>  </button>\n                            </ion-item-options> \n                          </ion-item-sliding>\n                           </ion-list>\n                     </ion-card-content>\n                    </ion-card>\n              </ion-col>\n               <!-- <ion-col col-2>\n                <button ion-button color="orange" class="chatBtn" (click)="opengroupChat(item.ProjName,item.Logo,item.ProjResources)">\n         <ion-icon name="chatboxes"></ion-icon>\n       </button>\n              </ion-col>  -->\n            </ion-row>\n          </ion-grid>\n \n  \n   <!-- <ion-list class="animated"  [ngClass]="{\'fadeInDown\':item.open,\'fadeOutUp\':!item.open}">\n    <ion-list *ngIf="item.ProjResources && item.open">\n       <ion-item-sliding #item *ngFor="let res of item.ProjResources" >\n          <ion-item *ngIf="res.username" (click)="openChat(res.username,res.imgSrc)" >\n              <ion-avatar item-start>\n                  <img [src]="res.imgSrc">\n                </ion-avatar>\n                <label>\n                  <h4>{{res.username}}</h4>\n                    <p>{{res.msg}}</p> \n                </label>\n                <ion-note item-end [ngClass]="{\'online\':res.status,\'offline\':!res.status}"></ion-note>\n          </ion-item>\n    <ion-item-options side="left">\n      <button ion-button color="balanced" large class="ionButton" ><ion-icon ios="ios-call" md="md-call" ></ion-icon></button>\n      <button ion-button large class="ionButton" ><ion-icon ios="ios-videocam" md="md-videocam" ></ion-icon></button>\n    </ion-item-options> \n    <ion-item-options side="right">\n      <button ion-button color="assertive" large class="ionButton" ><ion-icon name="trash"></ion-icon>  </button>\n    </ion-item-options> \n    \n  </ion-item-sliding>\n    </ion-list>\n   </ion-list> -->\n  </ion-item>\n   <ion-item-options side="right">\n      <button ion-button large clear (click)="opengroupChat(item.ProjName,item.Logo,item.ProjResources)">\n          <ion-icon name="chatboxes"></ion-icon>\n        </button>\n      <button ion-button large clear color="assertive"><ion-icon class="trashColor" name="trash"></ion-icon>  </button>\n    </ion-item-options>\n  </ion-item-sliding>\n </ion-list>\n</ion-list>\n\n<ion-fab left bottom >\n    <button ion-fab color="royal"><ion-icon name="arrow-dropright"></ion-icon></button>\n    <ion-fab-list side="right"><button ion-fab mini color="energized" (click)="openContacts()"><ion-icon name="contacts"></ion-icon></button>\n    <button ion-fab mini color="balanced" (click)="openModal()"><ion-icon name="add"></ion-icon></button>\n    <button ion-fab mini color="assertive" (click)="deleteProject()"><ion-icon name="remove"></ion-icon></button>\n    </ion-fab-list>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\project\project.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
    ], ProjectPage);
    return ProjectPage;
}());

//# sourceMappingURL=project.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng_socket_io__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng_socket_io__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the UsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UsersPage = /** @class */ (function () {
    function UsersPage(navCtrl, navParams, http, socket, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.socket = socket;
        this.modalCtrl = modalCtrl;
        this.userlist = [
            {
                "imgSrc": "assets/imgs/rose.jpg",
                "username": "Ramesh",
                "msg": "Hello"
            },
            {
                "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                "username": "Elan",
                "msg": "Hey"
            },
            {
                "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                "username": "Keerthana",
                "msg": "Hi Keerthana"
            },
            {
                "imgSrc": "assets/imgs/bike.jpg",
                "username": "Vicky",
                "msg": "Hi Buddy"
            },
            {
                "imgSrc": "assets/imgs/bike.jpg",
                "username": "Eliyas",
                "msg": "Hi Eliyas"
            },
            {
                "imgSrc": "assets/imgs/rose.jpg",
                "username": "Ajit",
                "msg": "Hello"
            },
            {
                "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                "username": "Merlin",
                "msg": "Hai Merlin"
            },
            {
                "imgSrc": "assets/imgs/bike.jpg",
                "username": "Shirly",
                "msg": "Hey !!!!"
            },
            {
                "imgSrc": "assets/imgs/bike.jpg",
                "username": "Abdul",
                "msg": "How r u man?"
            },
            {
                "imgSrc": "assets/imgs/rose.jpg",
                "username": "Ashik",
                "msg": "U thr dood?"
            },
            {
                "imgSrc": "assets/imgs/scarlet_tanager.jpg",
                "username": "Sowbhi",
                "msg": "Hai Akka"
            },
            {
                "imgSrc": "assets/imgs/bike.jpg",
                "username": "Lalli",
                "msg": "Hi Frnd"
            },
            {
                "imgSrc": "assets/imgs/bike.jpg",
                "username": "Poongundran",
                "msg": "Hai Sir"
            }
        ];
        this.nickname = this.navParams.get('username');
    }
    UsersPage.prototype.openModal = function () {
        var modal = this.modalCtrl.create('ModalPage', { Title: "Add Employee" });
        modal.onDidDismiss(function (data) {
            console.log(data);
            //this.items=data;
        });
        modal.present();
    };
    UsersPage.prototype.openChat = function (un, img) {
        this.socket.connect();
        this.socket.emit('set-nickname', un);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */], { nickname: un, imgPath: img });
    };
    UsersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UsersPage');
    };
    UsersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-users',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\users\users.html"*/'<!--\n  Generated template for the UsersPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="balanced">\n    <ion-title class="empheader">\n      <ion-item no-lines>\n          Employees\n          <ion-avatar item-end>\n              <img src="../../assets/imgs/baby.jpg">\n            </ion-avatar>\n      </ion-item>\n     \n    </ion-title>\n    \n   \n    <!-- <ion-buttons end>             \n        <ion-avatar item-end>\n            <img src="../../assets/imgs/baby.jpg">\n          </ion-avatar>\n      </ion-buttons>  -->\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <!-- <ion-list *ngFor="let item of userlist">\n        <ion-item-sliding #slidingItem>\n          <ion-item *ngIf="item.username!=nickname"  (click)="openChat(item.username,item.imgSrc)">\n              <ion-avatar item-start>\n                  <img [src]="item.imgSrc">\n                 </ion-avatar>\n                 <label>{{item.username}}</label>\n                 <p>{{item.msg}}</p>\n          </ion-item>\n          <ion-item-options side="right">\n              <button ion-button color="balanced"> Call <ion-icon ios="ios-call" md="md-call" ></ion-icon></button>\n              <button ion-button color="danger"> Video Call<ion-icon ios="ios-videocam" md="md-videocam" ></ion-icon></button>\n          </ion-item-options>\n          <ion-item-options side="left">            \n              <button ion-button color="danger"><ion-icon name="more" ></ion-icon></button>\n              <button ion-button color="balanced"><ion-icon name="add" ></ion-icon></button>\n          </ion-item-options>\n        </ion-item-sliding>\n      </ion-list> -->\n      <!-- <h2>Sample</h2> -->\n      <ion-list>\n          <ion-item-sliding *ngFor="let item of userlist">\n            <ion-item *ngIf="item.username!=nickname" (click)="openChat(item.username,item.imgSrc)">\n                <ion-avatar item-start>\n                    <img [src]="item.imgSrc">\n                  </ion-avatar>\n                  <label>{{item.username}}</label>\n                  <p>{{item.msg}}</p> \n            </ion-item>\n            <ion-item-options side="right">                \n                <button ion-button color="danger" clear large (click)="presentPopover($event)"><ion-icon name="more" ></ion-icon></button>\n              </ion-item-options> \n            <ion-item-options side="left">   \n                <button ion-button color="balanced" large class="ionButton" ><ion-icon ios="ios-call" md="md-call" ></ion-icon></button>\n                <button ion-button color="danger" large class="ionButton" ><ion-icon ios="ios-videocam" md="md-videocam" ></ion-icon></button>\n                <button ion-button color="assertive" large class="ionButton"><ion-icon name="add" ></ion-icon></button>\n            </ion-item-options>\n          </ion-item-sliding>\n        </ion-list>\n        \n\n\n<ion-fab right bottom>\n  <button ion-fab mini color="secondary" (click)="openModal()"><ion-icon name="add"></ion-icon></button>\n</ion-fab>\n</ion-content>\n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\users\users.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4_ng_socket_io__["Socket"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
    ], UsersPage);
    return UsersPage;
}());

//# sourceMappingURL=users.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_project_project__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_dash_board_dash_board__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_leave_leave__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_group_chat_group_chat__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_popover_popover__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_users_users__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_files_files__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, popoverCtrl, events) {
        var _this = this;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.footerIsHidden = false;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        events.subscribe('hideHeader', function (data) {
            _this.footerIsHidden = data.isHidden;
        });
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        //firebase.initializeApp(firebaseConfig);
        this.appMenuItems = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_6__pages_dash_board_dash_board__["a" /* DashBoardPage */], icon: 'fa fa-home' },
            { title: 'Messenger Forum', component: __WEBPACK_IMPORTED_MODULE_5__pages_project_project__["a" /* ProjectPage */], icon: 'fa fa-commenting' },
            { title: 'Work From Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_project_project__["a" /* ProjectPage */], icon: 'fa fa-laptop' },
            { title: 'Contacts', component: __WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__["a" /* ChatPage */], icon: 'fa fa-users' },
            { title: 'Leave of Absence', component: __WEBPACK_IMPORTED_MODULE_8__pages_leave_leave__["a" /* LeavePage */], icon: 'fa fa-pencil-square-o' },
            { title: 'Groups', component: __WEBPACK_IMPORTED_MODULE_9__pages_group_chat_group_chat__["a" /* GroupChatPage */], icon: 'fa fa-comments' }
        ];
        this.footerButtons = [
            { name: "home", component: __WEBPACK_IMPORTED_MODULE_6__pages_dash_board_dash_board__["a" /* DashBoardPage */] },
            { name: "contacts", component: __WEBPACK_IMPORTED_MODULE_11__pages_users_users__["a" /* UsersPage */] },
            { name: "briefcase", component: __WEBPACK_IMPORTED_MODULE_5__pages_project_project__["a" /* ProjectPage */] },
            { name: "folder", component: __WEBPACK_IMPORTED_MODULE_12__pages_files_files__["a" /* FilesPage */] },
        ];
    }
    MyApp.prototype.presentPopover = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_10__components_popover_popover__["a" /* PopoverComponent */]);
        popover.present({
            ev: myEvent
        });
    };
    MyApp.prototype.openFiles = function (page) {
        // this.nav.push(page);
        if (page === __WEBPACK_IMPORTED_MODULE_6__pages_dash_board_dash_board__["a" /* DashBoardPage */])
            this.nav.setRoot(page);
        else
            this.nav.push(page);
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component === __WEBPACK_IMPORTED_MODULE_6__pages_dash_board_dash_board__["a" /* DashBoardPage */])
            this.nav.setRoot(page.component);
        else
            this.nav.push(page.component);
    };
    MyApp.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashBoardPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\app\app.html"*/'<ion-menu [content]="mycontent" class="menu">\n  <ion-header>\n    <ion-toolbar color="balanced">      \n      <ion-grid>\n        <ion-row no-padding>\n          <ion-col col-3 no-padding>\n              <div class="avatar">\n                  <img  src="../assets/imgs/baby.jpg">\n              </div>           \n          </ion-col>\n          <ion-col col-9 no-padding>\n            <ion-row no-padding>\n              <ion-col col-10 no-padding>\n                  <h2 ion-text class="margin-top10px">\n                      Elan\n                    </h2>\n              </ion-col>\n              <ion-col col-2 no-padding>\n                  <button class="edit" ion-button clear (click)="presentPopover($event)"><ion-icon class="edit" name="create"></ion-icon></button>\n              </ion-col>\n              <ion-col col-12 no-padding>\n                  <label class="swengr">Sr.Software Engineer</label>                   \n              </ion-col>\n            </ion-row>            \n          </ion-col>              \n        </ion-row>       \n        <ion-row no-padding>\n          <ion-col no-padding>\n              <ion-item class="meet" no-padding no-lines>                             \n                      <ion-label color="light">In a Meeting</ion-label>       \n                  <ion-checkbox no-padding color="danger"margin-left></ion-checkbox>                  \n                </ion-item>\n          </ion-col>  \n          <ion-col no-padding class="logout">\n              <button ion-button  clear  color="light">\n                  <ion-icon color="light" name="log-out"></ion-icon>   \n                  Logout                   \n                </button>\n          </ion-col>       \n        </ion-row>\n      </ion-grid>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content color="primary">\n      <ion-list no-lines>\n        <button ion-item menuClose class="menuPages" *ngFor="let menuItem of appMenuItems" (click)="openPage(menuItem)">\n          <ion-icon item-left class="{{menuItem.icon}} cardFont" color="light"></ion-icon>\n          <span ion-text >{{menuItem.title}}</span>\n        </button>\n      </ion-list>\n    </ion-content>\n  </ion-menu>  \n  <ion-footer class="commonFooter" *ngIf="!footerIsHidden">\n    <ion-toolbar color="balanced">\n        <ion-buttons>  \n          <button ion-button class="footerButtonpadding" *ngFor="let footerIcon of footerButtons" (click)="openFiles(footerIcon.component)">\n              <ion-icon [name]="footerIcon.name" class="homeBtn"></ion-icon>\n          </button>          \n            <button item-end ion-button>\n                <ion-icon class="homeBtn" name="search"></ion-icon>\n            </button>            \n          </ion-buttons>   \n    </ion-toolbar>\n  </ion-footer>\n<ion-nav #mycontent [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashBoardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__project_project__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__users_users__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__files_files__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__leave_leave__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the DashBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashBoardPage = /** @class */ (function () {
    function DashBoardPage(menu, navCtrl, navParams) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toggleNotify = true;
        menu.enable(true);
        this.cardMenus = [
            {
                title: "Messenger Forum",
                icon: "fa fa-commenting",
                color: "messenger",
                Component: __WEBPACK_IMPORTED_MODULE_2__project_project__["a" /* ProjectPage */]
            },
            {
                title: "Work From Home",
                icon: "fa fa-home",
                color: "home"
            },
            {
                title: "Profile",
                icon: "fa fa-user",
                color: "profile"
            },
            {
                title: "Leave of Absence",
                icon: "fa fa-pencil-square-o",
                color: "loa",
                Component: __WEBPACK_IMPORTED_MODULE_5__leave_leave__["a" /* LeavePage */]
            },
            {
                title: "Project",
                icon: "fa fa-tasks",
                color: "project"
            },
            {
                title: "Contacts",
                icon: "fa fa-users",
                color: "contacts",
                Component: __WEBPACK_IMPORTED_MODULE_3__users_users__["a" /* UsersPage */]
            }
        ];
        this.recent = [
            {
                profpic: "assets/imgs/rose.jpg",
                name: "Swetha"
            },
            {
                profpic: "assets/imgs/bike.jpg",
                name: "Ramesh"
            },
            {
                profpic: "assets/imgs/scarlet_tanager.jpg",
                name: "Lalli"
            },
            {
                profpic: "assets/imgs/motivate.jpg",
                name: "Bala"
            },
            {
                profpic: "assets/imgs/rose.jpg",
                name: "Padhu"
            },
            {
                profpic: "assets/imgs/bike.jpg",
                name: "Michael"
            },
            {
                profpic: "assets/imgs/motivate.jpg",
                name: "Vignesh"
            },
            {
                profpic: "assets/imgs/baby.jpg",
                name: "Sowbhi"
            }
        ];
        this.footerButtons = [
            { name: "home", component: __WEBPACK_IMPORTED_MODULE_3__users_users__["a" /* UsersPage */] },
            { name: "contacts", component: __WEBPACK_IMPORTED_MODULE_3__users_users__["a" /* UsersPage */] },
            { name: "briefcase", component: __WEBPACK_IMPORTED_MODULE_2__project_project__["a" /* ProjectPage */] },
            { name: "folder", component: __WEBPACK_IMPORTED_MODULE_4__files_files__["a" /* FilesPage */] },
        ];
    }
    DashBoardPage.prototype.toggleNotifications = function () {
        this.toggleNotify = !this.toggleNotify;
    };
    DashBoardPage.prototype.openFiles = function (page) {
        this.navCtrl.push(page);
    };
    DashBoardPage.prototype.navigateComponent = function (page) {
        if (page.Component)
            this.navCtrl.push(page.Component);
    };
    DashBoardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashBoardPage');
    };
    DashBoardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dash-board',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\dash-board\dash-board.html"*/'<!--\n  Generated template for the DashBoardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="balanced" hideBackButton>\n      <button ion-button icon-only menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title text-center >Dashboard</ion-title> \n    <ion-buttons end>\n      <button ion-button class="headerButtonpadding" (click)="toggleNotifications()">\n          <ion-icon name="notifications" class="homeBtn"></ion-icon>\n      </button>\n        <button ion-button>\n            <ion-icon name="settings" class="homeBtn"></ion-icon> \n        </button> \n    </ion-buttons>       \n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>    \n  <ion-grid>\n    <ion-row *ngIf="toggleNotify">\n        <ion-card class="notify_Card" [ngClass]="{\'notify_CardShow\':toggleNotify,\'notify_CardHide\':!toggleNotify}">\n            <ion-card-header>\n              <ion-item>\n                <strong>NHVRIN<i>plus</i></strong>\n                <ion-note item-end (click)="toggleNotifications()"><ion-icon name="close"></ion-icon></ion-note>\n              </ion-item> \n              \n            </ion-card-header>\n            <ion-card-content>\n              You are dedicated as 0.5 resource in NHVRIN<i>plus</i>\n            </ion-card-content>\n          </ion-card>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6  *ngFor="let menu of cardMenus; let i=index" (click)="navigateComponent(menu)">\n          <ion-card text-center class="animateDelay" [ngClass]="{\'animate-cardleft\':i%2!=0,\'animate-cardright\':i%2==0}">    \n              <!-- class="animate-cardright" [ngClass]="{\'animate-cardleft\':i%2!=0,\'animate-cardright\':i%2==0}" -->\n              <ion-card-content>\n                <ion-card-title  >\n                    <!-- <ion-icon [name]="menu.icon" class="cardFont"></ion-icon> -->\n                    <button ion-button large clear [color]="menu.color">\n                        <ion-icon class="{{menu.icon}} cardFont"  ></ion-icon>\n                    </button>\n                    \n                </ion-card-title>               \n              </ion-card-content>          \n              <ion-row no-padding>\n                <ion-col >\n                    <label>{{menu.title}}</label>\n                </ion-col>               \n              </ion-row>          \n            </ion-card>\n      </ion-col>      \n    </ion-row>\n  </ion-grid>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-item class="recent">Recents</ion-item>\n          <ion-slides>\n            <ion-slide class="dummy" item-start *ngFor="let recentItem of recent" style="width:70px;">\n              <ion-item no-lines text-center>\n                  <ion-avatar item-start>\n                      <img [src]="recentItem.profpic">\n                     <span class="recentFont">{{recentItem.name}}</span> \n                    </ion-avatar>\n              </ion-item>                \n            </ion-slide>\n          </ion-slides>\n          <!-- <ion-item no-lines>\n              <ion-avatar item-start *ngFor="let recentItem of recent">\n                  <img [src]="recentItem.profpic">\n                  {{recentItem.name}}\n                </ion-avatar>\n                \n          </ion-item> -->\n          \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n</ion-content>\n<!-- <ion-footer>\n    <ion-toolbar color="balanced">\n        <ion-buttons>  \n          <button ion-button class="footerButtonpadding" *ngFor="let footerIcon of footerButtons" (click)="openFiles(footerIcon.component)">\n              <ion-icon [name]="footerIcon.name" class="homeBtn"></ion-icon>\n          </button>       \n          //Dummy Start    \n            <button ion-button class="footerButtonpadding">\n                <ion-icon name="home" class="homeBtn"></ion-icon>\n            </button>\n            <button ion-button class="footerButtonpadding">\n                <ion-icon name="contacts" class="homeBtn"></ion-icon>\n              </button>\n            <button ion-button class="footerButtonpadding">\n                <ion-icon name="briefcase" class="homeBtn"></ion-icon>\n            </button>  \n             <button ion-button class="footerButtonpadding" (click)="openFiles()">\n              <ion-icon name="folder" class="homeBtn"></ion-icon>\n            </button>  \n            //Dummy End                 \n            <button item-end ion-button>\n                <ion-icon class="homeBtn" name="search"></ion-icon>\n            </button>            \n          </ion-buttons>   \n    </ion-toolbar>\n  </ion-footer> -->'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\dash-board\dash-board.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], DashBoardPage);
    return DashBoardPage;
}());

//# sourceMappingURL=dash-board.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GroupChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GroupChatPage = /** @class */ (function () {
    function GroupChatPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nickname = '';
        this.imgPath = '';
        this.nickname = this.navParams.get('nickname');
        this.imgPath = this.navParams.get('imgPath');
        this.grpMembers = this.navParams.get('users');
    }
    GroupChatPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GroupChatPage');
    };
    GroupChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-group-chat',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\group-chat\group-chat.html"*/'<ion-header>\n    <ion-navbar color="balanced">\n        <ion-title>\n            <ion-grid>\n                <ion-row>              \n                  <ion-col col-2>\n                      <ion-avatar>\n                          <img [src]="imgPath"  class="ion-avatarClass">\n                      </ion-avatar>           \n                  </ion-col>          \n                  <ion-col col-6 >\n                    <label class="text">{{nickname}}</label><br>\n                    <label class="lastSeen">{{this.grpMembers}}</label> \n                                       \n                  </ion-col>\n                  <ion-col col-2  class="textpadding">            \n                      <ion-icon ios="ios-call" md="md-call" ></ion-icon>\n                  </ion-col>\n                  <ion-col col-2 class="textpadding">\n                      <ion-icon ios="ios-videocam" md="md-videocam" ></ion-icon>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>                         \n        </ion-title>\n      </ion-navbar>\n    </ion-header>\n    <ion-content class="chat">\n            <ion-grid>\n                    <ion-row *ngFor="let message of messages">      \n                      <ion-col col-9 *ngIf="message.from !== nickname" class="message" [ngClass]="{\'my_message\': message.from === nickname, \'other_message\': message.from !== nickname}">\n                        <div class="user_name">{{ message.from }}</div>                         \n                        <span>{{ message.text }}</span>\n                        <span class="time">{{message.created | date:\'hh:MM:ss\'}}</span>\n                      </ion-col>\n                  \n                      <ion-col offset-3 col-9 *ngIf="message.from === nickname" class="message" [ngClass]="{\'my_message\': message.from === nickname, \'other_message\': message.from !== nickname}">\n                        <div class="user_name">Me</div>\n                        <span>{{ message.text }}</span>\n                        <span class="time">{{message.created | date:\'hh:MM:ss\'}}</span>\n                      </ion-col>                  \n                    </ion-row>\n                  </ion-grid>\n        <ion-fab right bottom>\n            <button ion-fab color="royal"><ion-icon name="arrow-dropleft"></ion-icon></button>\n            <ion-fab-list side="left">\n              <button ion-fab><ion-icon name="attach"></ion-icon></button>\n              <button ion-fab><ion-icon name="camera"></ion-icon></button>\n              <button ion-fab><ion-icon name="happy"></ion-icon></button>\n            </ion-fab-list>\n          </ion-fab>\n    </ion-content>   \n  <ion-footer>\n      <ion-grid class="inputmessage">\n          <ion-row class="input-Class">\n            <ion-col col-11>\n                <ion-input type="text"  placeholder="Type a message" [(ngModel)]="message"></ion-input>\n            </ion-col>\n            <ion-col col-1 class="textpadding">                \n                  <button class="sendBtn" [disabled]="message === \'\'" (click)="sendMessage()"><ion-icon name="send"  class="sendIcon"></ion-icon></button>                   \n            </ion-col>\n          </ion-row>\n        </ion-grid>\n  </ion-footer>\n        \n      \n  '/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\group-chat\group-chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], GroupChatPage);
    return GroupChatPage;
}());

//# sourceMappingURL=group-chat.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FilesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FilesPage = /** @class */ (function () {
    function FilesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FilesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilesPage');
    };
    FilesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-files',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\files\files.html"*/'<!--\n  Generated template for the FilesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="balanced">\n    <ion-title padding-left>Files</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\files\files.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], FilesPage);
    return FilesPage;
}());

//# sourceMappingURL=files.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeavePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import * as firebase from 'firebase';
//import { snapshotToArray } from '../../app/environment'
/**
 * Generated class for the LeavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LeavePage = /** @class */ (function () {
    function LeavePage(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.date = false;
        this.items = [];
        //ref=firebase.database().ref('items/');
        this.inputText = '';
        // this.ref.on('value',resp=>{
        //   this.items=snapshotToArray(resp);
        // });
        this.leaveofabsence = this.formBuilder.group({
            LeaveType: '',
            NOD: '',
            OneDay: '',
            FromDate: '',
            ToDate: '',
        });
        this.details = [
            {
                leavetype: 'Casual',
                days: '10',
                leavedate: '-',
                from: '10/01/2019',
                to: '10/10/2019'
            },
            {
                leavetype: 'Sick',
                days: '1',
                leavedate: '03/01/2019',
                from: '-',
                to: '-'
            },
            {
                leavetype: 'Maternity',
                days: '1',
                leavedate: '10/01/2019',
                from: '-',
                to: '-'
            },
            {
                leavetype: 'Paternity',
                days: '10',
                leavedate: '-',
                from: '10/01/2019',
                to: '10/10/2019'
            },
            {
                leavetype: 'Casual',
                days: '10',
                leavedate: '-',
                from: '10/01/2019',
                to: '10/10/2019'
            },
            {
                leavetype: 'Casual',
                days: '10',
                leavedate: '-',
                from: '10/01/2019',
                to: '10/10/2019'
            },
            {
                leavetype: 'Casual',
                days: '10',
                leavedate: '-',
                from: '10/01/2019',
                to: '10/10/2019'
            },
            {
                leavetype: 'Casual',
                days: '10',
                leavedate: '-',
                from: '10/01/2019',
                to: '10/10/2019'
            },
            {
                leavetype: 'Casual',
                days: '10',
                leavedate: '-',
                from: '10/01/2019',
                to: '10/10/2019'
            }
        ];
    }
    LeavePage.prototype.leaveForm = function (leave, nod, OneDay, from, to) {
        if (OneDay) {
            var onedayDate = __WEBPACK_IMPORTED_MODULE_3_moment___default()(OneDay).format('MM/DD/YYYY');
            var fromDate = '-';
            var toDate = '-';
        }
        else {
            onedayDate = '-';
            fromDate = __WEBPACK_IMPORTED_MODULE_3_moment___default()(from).format('MM/DD/YYYY');
            toDate = __WEBPACK_IMPORTED_MODULE_3_moment___default()(to).format('MM/DD/YYYY');
            var a = __WEBPACK_IMPORTED_MODULE_3_moment___default()(from);
            var b = __WEBPACK_IMPORTED_MODULE_3_moment___default()(to);
            nod = b.diff(a, 'days');
        }
        var obj = {
            leavetype: leave,
            days: nod,
            leavedate: onedayDate,
            from: fromDate,
            to: toDate
        };
        this.details.push(obj);
        this.leaveofabsence.reset();
    };
    // addItem(item){
    //   if(item!==undefined && item!==null){
    //     let newItem=this.ref.push();
    //     newItem.set(item);
    //     this.inputText='';
    //   }   
    // }
    LeavePage.prototype.daycount = function (nod) {
        if (nod == 1)
            this.date = false;
        else
            this.date = true;
    };
    LeavePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LeavePage');
    };
    LeavePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave',template:/*ion-inline-start:"e:\IONIC POC\CHAT-APP\src\pages\leave\leave.html"*/'<!--\n  Generated template for the LeavePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="balanced">\n    <ion-title padding-left >Leave of Absence</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding> \n  <!-- <ion-input [(ngModel)]="inputText" placehoder="Name"></ion-input>\n  <button ion-button full (click)="addItem({name:inputText})">Add</button>\n  <ion-list>\n    <ion-item *ngFor="let item of items">{{item.name}}</ion-item>\n  </ion-list> -->\n  <ion-card class="formLOA">\n            \n    <ion-card-content no-padding>\n        <form [formGroup]="leaveofabsence" (ngSubmit)="leaveForm(leave,nod,OneDay,from,to)">\n      <ion-list> \n        <ion-item >\n            <ion-label>Type of Leave</ion-label>\n            <ion-select formControlName="LeaveType" [(ngModel)]="leave" interface="popover" placeholder="Select">\n              <ion-option value="Casual">Casual Leave</ion-option>\n              <ion-option value="Sick">Sick Leave</ion-option>\n              <ion-option value="Maternity">Maternity Leave</ion-option>\n              <ion-option value="Paternity">Paternity Leave</ion-option>\n            </ion-select>\n          </ion-item>  \n        </ion-list> \n        <ion-list>\n            <ion-item>\n              <ion-label>Number of Days</ion-label>             \n              <ion-select formControlName="NOD" [(ngModel)]="nod" interface="popover" (ionChange)="daycount(nod)" placeholder="Select">\n                <ion-option value="1">1</ion-option>\n                <ion-option value="Greater">>1</ion-option>\n\n              </ion-select>\n            </ion-item>\n          </ion-list>\n          <ion-item *ngIf="!date">\n              <ion-label>Date of Leave</ion-label>\n              <ion-datetime formControlName="OneDay" displayFormat="MM/DD/YYYY" [(ngModel)]="OneDay" placeholder="MM/DD/YYYY"></ion-datetime>\n            </ion-item>\n      <ion-grid *ngIf="date" no-padding>\n        <ion-row no-padding>        \n              <ion-item>\n                  <ion-label>From</ion-label>\n                  <ion-datetime formControlName="FromDate" displayFormat="MM/DD/YYYY" [(ngModel)]="from" placeholder="MM/DD/YYYY"></ion-datetime>\n                </ion-item>\n              </ion-row>\n                <ion-row>     \n              <ion-item>\n                  <ion-label>To</ion-label>\n                  <ion-datetime formControlName="ToDate" displayFormat="MM/DD/YYYY" [(ngModel)]="to" placeholder="MM/DD/YYYY"></ion-datetime>\n                </ion-item>         \n        </ion-row>\n      </ion-grid> \n      <ion-grid>\n          <ion-row >\n              <button type="submit" ion-button block color="energized">\n                  Submit\n                </button> \n          </ion-row>\n      </ion-grid>  \n          </form>\n    </ion-card-content>\n  </ion-card>\n  <ion-card no-padding class="formDetails">\n    <ion-card-header no-padding class="headerborderbottom">\n      <ion-list>\n        <ion-item no-padding>\n          <ion-label color="primary">Leave</ion-label>\n          <ion-label color="primary">#Days</ion-label>\n          <ion-label color="primary">Leave Date</ion-label>\n          <ion-label color="primary">From</ion-label>\n          <ion-label color="primary">To</ion-label>\n        </ion-item>\n      </ion-list>\n      \n    </ion-card-header>\n    \n    <ion-card-content class="insideDetails">  \n            <ion-scroll scrollY="true">\n                <ion-list>\n                    <ion-item no-padding *ngFor="let formdetails of details" class="borderbottom">\n                      <ion-label>{{ formdetails.leavetype }}</ion-label>\n                      <ion-label>{{ formdetails.days }}</ion-label>\n                      <ion-label>{{formdetails.leavedate}}</ion-label>  \n                      <ion-label>{{formdetails.from}}</ion-label>\n                      <ion-label>{{formdetails.to}}</ion-label> \n                    </ion-item>\n                    </ion-list>                \n            </ion-scroll>     \n    </ion-card-content>\n  </ion-card>\n  <ion-card no-padding class="leaveCount">\n    <ion-card-header no-padding class="headerborderbottom">\n      <ion-list>\n        <ion-item no-padding>\n          <ion-label color="primary">Total Leaves</ion-label>\n          <ion-label color="primary">Leaves Taken</ion-label>\n          <ion-label color="primary">Leaves Pending</ion-label>\n        </ion-item>\n      </ion-list>\n    </ion-card-header>\n    <ion-card-content>\n    <ion-list>\n      <ion-item no-padding>\n      <ion-label>24</ion-label>\n      <ion-label>10</ion-label>\n      <ion-label>14</ion-label>\n    </ion-item>\n  </ion-list>\n    </ion-card-content>\n  </ion-card>\n    \n    \n</ion-content>\n'/*ion-inline-end:"e:\IONIC POC\CHAT-APP\src\pages\leave\leave.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], LeavePage);
    return LeavePage;
}());

//# sourceMappingURL=leave.js.map

/***/ })

},[379]);
//# sourceMappingURL=main.js.map